from mysql.connector import connect

class database:
    def __init__(self):
        try:
            self.db = connect(host='localhost',
                            database='perpustakaan',
                            user='root',
                            password='PasswSQL156#')
        except Exception as e:
            print(e)
    
    def showBorrowByEmail(self, **params):
        cursor = self.db.cursor()
        query = f'''
        select customers.username, borrows.*
        from borrows
        inner join customers on borrows.userid = customers.userid
        where customers.email = "{params["email"]}" and borrows.isactive = 1;
        '''
        cursor.execute(query)
        result = cursor.fetchall()
        return result

    def insertBorrow(self, **params):
        column = ', '.join(list(params.keys()))
        values = tuple(list(params.values()))
        cursor = self.db.cursor()
        query = f'''
        insert into borrows ({column})
        values {values};
        '''
        cursor.execute(query)
        

    def updateBorrow(self, **params):
        borrowid = params["borrowid"]
        cursor = self.db.cursor()
        query = f'''
        update borrows
        set isactive = 0
        where borrorid = {borrowid};
        '''
        cursor.execute(query)


    def dataCommit(self):
        self.db.commit()